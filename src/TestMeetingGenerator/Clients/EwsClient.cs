﻿using System;
using System.Linq;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Identity.Client;
using Newtonsoft.Json.Linq;
using TestMeetingGenerator.DataObject;
using MeetingRequest = TestMeetingGenerator.DataObject.MeetingRequest;

namespace TestMeetingGenerator.Clients
{
    public class EwsClient
    {
        private readonly string _ewsServiceUrl;
        private readonly string _ewsCredentials;
        private readonly string _domain;
        private static readonly string[] EwsScopes = new[] { "https://outlook.office365.com/.default" };

        public EwsClient(string ewsServiceUrl, string ewsCredentials, string domain)
        {
            _ewsServiceUrl = ewsServiceUrl;
            _ewsCredentials = ewsCredentials;
            _domain = domain;
        }

        public void CreateAppointment(MeetingRequest meeting, User user)
        {
            var exchangeService = CreateExchangeService(user);

            var appointment = new Appointment(exchangeService)
            {
                Subject = meeting.Meeting.EventName
            };

            var room = meeting.Booking.Rooms.First();

            appointment.Start = room.Start.DateTime.Value;
            appointment.End = room.End.DateTime.Value;

            var roomMailbox = GetRoomMailbox(room.RoomKey);
            appointment.Resources.Add(roomMailbox);

            appointment.Save(SendInvitationsMode.SendToAllAndSaveCopy);
        
            var item = Item.Bind(exchangeService, appointment.Id, new PropertySet(ItemSchema.Subject));
            Console.WriteLine("\nAppointment created: " + item.Subject + " in room " + room.RoomKey + " \n");
        }

        private ExchangeService CreateExchangeService(User user)
        {
            var service = new ExchangeService(ExchangeVersion.Exchange2010_SP2)
            {
                Url = new Uri(_ewsServiceUrl),
                ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, user.Username)
            };

            service.HttpHeaders.Add("X-AnchorMailbox", user.Username);
            var parameters = DecryptAndParseCredentialParameters(_ewsCredentials);
            var applicationToken = GetO365ApplicationAuthenticationToken(parameters);
            service.Credentials = new OAuthCredentials(applicationToken.AccessToken);

            return service;
        }

        private AuthenticationResult GetO365ApplicationAuthenticationToken(dynamic parameters)
        {
            var clientId = parameters.ClientId.ToString() as string;
            var tenantId = parameters.TenantId.ToString() as string;

            var msalApplicationBuilder = (
                ConfidentialClientApplicationBuilder
                    .Create(clientId)
                    .WithTenantId(tenantId)
            );

            var secret = parameters.Secret.ToString() as string;
            msalApplicationBuilder = msalApplicationBuilder.WithClientSecret(secret);

            var msal = msalApplicationBuilder.Build();

            var token = (
                msal.AcquireTokenForClient(EwsScopes)
                    .ExecuteAsync()
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult()
            );

            return token;
        }

        private dynamic DecryptAndParseCredentialParameters(string credentialParameters)
        {
            dynamic parameters = JObject.Parse(credentialParameters);
            return parameters;
        }

        private string GetRoomMailbox(string roomKey)
        {
            return $"test.room{roomKey}" + _domain;
        }
    }
}
