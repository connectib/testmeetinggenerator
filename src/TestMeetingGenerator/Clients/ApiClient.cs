﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TestMeetingGenerator.DataObject;

namespace TestMeetingGenerator.Clients
{
    public class ApiClient
    {
        private readonly string _searchAttendeeUrl;
        private readonly string _authenticationUrl;
        private readonly string _saveMeetingUrl;
        public ApiClient(string baseUrl)
        {
            _authenticationUrl = baseUrl + "identity/api/IdentityServer/Login/UsernamePassword";
            _searchAttendeeUrl = baseUrl + "roombookingmanager/api/RoomBooking/Book/SearchAttendee";
            _saveMeetingUrl = baseUrl + "roombookingmanager/api/RoomBooking/Book/SaveMultiRoomMeetingDetail";
        }

        public async Task<string> GetTokenAsync(User user)
        {
            using (var httpClient = new HttpClient())
            {
                var stringContent = JsonConvert.SerializeObject(user);
                var httpResponse = await httpClient.PostAsync(_authenticationUrl, new StringContent(stringContent));
                var responseContentString = await httpResponse.Content.ReadAsStringAsync();

                var tokenObject = JsonConvert.DeserializeObject<TokenObject>(responseContentString);
                return tokenObject.Token;
            }
        }

        public async Task SaveMeetingAsync(MeetingRequest request, string token)
        {
            var serializedRequest = JsonConvert.SerializeObject(request);
            var stringContent = new StringContent(serializedRequest);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", token);
                client.DefaultRequestHeaders.Add("Connect-Type", "SaveMultiRoomMeetingDetail");
                var response = await client.PostAsync(_saveMeetingUrl, stringContent);

                if (!response.IsSuccessStatusCode)
                    throw new Exception("Meeting creation failed");
            }
        }

        public async Task<SearchAttendeeAttendee> SearchAttendeeAsync(string username, string token)
        {
            var request = new SearchAttendeeRequest
            {
                SearchTerm = username
            };
            var serializedRequest = JsonConvert.SerializeObject(request);
            var stringContent = new StringContent(serializedRequest);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", token);
                client.DefaultRequestHeaders.Add("Connect-Type", "SearchAttendee");
                var response = await client.PostAsync(_searchAttendeeUrl, stringContent);

                var responseContent = await response.Content.ReadAsStringAsync();

                var searchAttendeeResponse = JsonConvert.DeserializeObject<SearchAttendeeResponse>(responseContent);

                if (searchAttendeeResponse.Attendees == null || !searchAttendeeResponse.Attendees.Any())
                    throw new Exception($"Fail to get attendee {username}");

                return searchAttendeeResponse.Attendees.First();
            }
        }
    }
}
