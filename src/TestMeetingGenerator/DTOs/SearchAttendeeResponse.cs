﻿using System.Collections.Generic;

namespace TestMeetingGenerator.DataObject
{
    public class SearchAttendeeResponse
    {
        public List<SearchAttendeeAttendee> Attendees { get; set; }
    }

    public class SearchAttendeeAttendee
    {
        public string AttendeeKey { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
    }
}
