﻿namespace TestMeetingGenerator.DataObject
{
    public class SearchAttendeeRequest
    {
        public string SearchTerm { get; set; }
    }
}
