﻿using System;
using System.Collections.Generic;

namespace TestMeetingGenerator.DataObject
{
    public class MeetingRequest
    {
        public string Version { get; set; } = "1";
        public Meeting Meeting { get; set; }
        public Booking Booking { get; set; }
    }

    public class Meeting
    {
        public string ContactEmail { get; set; }
        public string ContactNumber { get; set; } = "1234567890";
        public string EventName { get; set; }
        public bool IsCurrentUserAttendee { get; set; } = true;
        public bool IsCurrentUserOrganiser { get; set; } = true;
        public string OrganiserKey { get; set; }
    }

    public class Booking
    {
        public List<Room> Rooms { get; set; } = new List<Room>();
    }

    public class Room
    {
        public string RoomKey { get; set; }

        public DateTimeInZone Start { get; set; } = new DateTimeInZone();

        public DateTimeInZone End { get; set; } = new DateTimeInZone();

        public List<string> Attendees { get; set; } = new List<string>();

    }

    public class DateTimeInZone
    {
        public DateTime? DateTime { get; set; }

        public string TimeZoneId { get; set; } = "GMT Standard Time";
    }
}
