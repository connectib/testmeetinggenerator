﻿using System;
using System.Configuration;
using TestMeetingGenerator.Generators;

namespace TestMeetingGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            var exchangeUrl = ConfigurationManager.AppSettings["ExchangeUrl"];
            var exchangeCredentials = ConfigurationManager.AppSettings["ExchangeCredentials"];
            var roomsAmount = int.Parse(ConfigurationManager.AppSettings["RoomsAmount"]);
            var usersAmount = int.Parse(ConfigurationManager.AppSettings["UsersAmount"]);
            var meetingCreationDelay = int.Parse(ConfigurationManager.AppSettings["MeetingCreationDelayInSeconds"]);
            var domain = ConfigurationManager.AppSettings["Domain"];

            Console.WriteLine("Type O for if you want to generate meetings in ORB, or type E to generate directly in Exchange");
            var rightChose = false;

            while (!rightChose)
            {
                var userInput = Console.ReadLine();
                if (string.Equals(userInput, "O", StringComparison.OrdinalIgnoreCase))
                {
                    rightChose = true;
                    Console.WriteLine("Generating meetings in ORB");

                    var orbMeetingGenerator = new OrbMeetingGenerator(baseUrl, roomsAmount, usersAmount,
                        domain, meetingCreationDelay);
                    orbMeetingGenerator.Start();
                }
                else if (string.Equals(userInput, "E", StringComparison.OrdinalIgnoreCase))
                {
                    rightChose = true;
                    Console.WriteLine("Generating meetings in Exchange");

                    var ewsMeetingGenerator = new EwsMeetingGenerator(exchangeUrl, roomsAmount, usersAmount, domain,
                        exchangeCredentials, meetingCreationDelay);
                    ewsMeetingGenerator.Start();
                }
                else
                {
                    Console.WriteLine("Please try again");
                }
            }
        }
    }
}
