﻿using System;
using TestMeetingGenerator.DataObject;

namespace TestMeetingGenerator.Helpers
{
    public class UserHelper
    {
        private readonly int _usersAmount;
        private readonly string _usersDomain;
        public UserHelper(int usersAmount, string usersDomain)
        {
            _usersAmount = usersAmount;
            _usersDomain = usersDomain;
        }

        public User GetRandomUser()
        {
            var random = new Random();
            var userIndex = random.Next(1, _usersAmount);
            return new User
            {
                Username = $"test.user{userIndex}" + _usersDomain,
                Password = "Connect1b"
            };
        }
    }
}
