﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestMeetingGenerator.Clients;
using TestMeetingGenerator.DataObject;
using TestMeetingGenerator.Helpers;

namespace TestMeetingGenerator.Generators
{
    public class EwsMeetingGenerator
    {
        private readonly EwsClient _ewsClient;
        private readonly UserHelper _userHelper;
        private readonly int _roomsAmount;
        private readonly int _meetingCreationDelay;

        public EwsMeetingGenerator(string ewsServiceUrl, 
            int roomsAmount, 
            int usersAmount, 
            string domain, 
            string ewsCredentials,
            int meetingCreationDelay)
        {
            _ewsClient = new EwsClient(ewsServiceUrl, ewsCredentials, domain);
            _userHelper = new UserHelper(usersAmount, domain);
            _roomsAmount = roomsAmount;
            _meetingCreationDelay = meetingCreationDelay;
        }

        public void Start()
        {
            Task.Run(StartAsync).GetAwaiter().GetResult();
        }

        public async Task StartAsync()
        {
            var tasks = new Task[_roomsAmount];
            for (var i = 0; i < _roomsAmount; i++)
            {
                var roomIndex = i + 1;
                var task = Task.Run(() => StartGenerationForRoom(roomIndex));
                tasks[i] = task;
            }

            await Task.WhenAll(tasks);
        }

        private async Task StartGenerationForRoom(int roomIndex)
        {
            var roomKey = roomIndex.ToString();
            var now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, now.Day + 1);
            DateTime.SpecifyKind(startDate, DateTimeKind.Local);

            var bookingWindowEndDate = startDate.AddDays(180);

            while (startDate < bookingWindowEndDate)
            {
                var user = _userHelper.GetRandomUser();
                var meetingRequest = CreateMeetingRequest(roomKey, user, startDate);

                _ewsClient.CreateAppointment(meetingRequest, user);

                await Task.Delay(TimeSpan.FromSeconds(_meetingCreationDelay));
                startDate = startDate.AddHours(1);
            }
        }

        private MeetingRequest CreateMeetingRequest(string roomKey, User user, DateTime start)
        {
            return new MeetingRequest
            {
                Meeting = new Meeting
                {
                    ContactEmail = user.Username,
                    EventName = $"meeting_{start.ToShortTimeString()}",
                },
                Booking = new Booking
                {
                    Rooms = new List<Room>
                    {
                        new Room
                        {
                            RoomKey = roomKey,
                            Start = new DateTimeInZone
                            {
                                DateTime = start
                            },
                            End = new DateTimeInZone
                            {
                                DateTime = start.AddHours(1)
                            }
                        }
                    }
                }
            };
        }
    }
}
